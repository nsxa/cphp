#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;

// Programa principal
int main()
{
      unsigned int elements=100000;
    	unsigned int operations=1000;
      
      int *a = new int[elements];
    	int *b = new int[elements];
    	
    	for (unsigned int i=0; i<elements; i++)
    	{
        a[i] = i;
        b[i] = i;
    	}
    	
		for (unsigned int j=0; j<operations; j++)
       		for (unsigned int i=0; i<elements; i++)
            	a[i] += b[i];
    
   return 0;
}
